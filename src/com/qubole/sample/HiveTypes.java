package com.qubole.sample;

/**
 * Defines conversion between SQL types and Hive 
 */
public final class HiveTypes {

  //public static final Log LOG = LogFactory.getLog(Hiveclass.getName());

  private HiveTypes() { }
  
  public static String toHiveType(String sqlType) {
	  	  
	  sqlType = sqlType.split("\\(")[0];
	  sqlType = sqlType.toUpperCase();

	  
      switch (sqlType) {
          case "INT":
          case "SMALLINT":
              return "INT";
          case "VARCHAR":
          case "CHAR":
          case "LONGVARCHAR":
          case "NVARCHAR":
          case "NCHAR":
          case "LONGNVARCHAR":
          case "DATE":
          case "TIME":
          case "TIMESTAMP":
          case "CLOB":
              return "STRING";
          case "NUMERIC":
          case "DECIMAL":
          case "FLOAT":
          case "DOUBLE":
          case "REAL":
              return "DOUBLE";
          case "BIT":
          case "BOOLEAN":
              return "BOOLEAN";
          case "TINYINT":
              return "TINYINT";
          case "BIGINT":
              return "BIGINT";
          default:
        	  return "STRING";
      }
  } 
 
}
