package com.qubole.sample;

import gudusoft.gsqlparser.EDbVendor;
import gudusoft.gsqlparser.ESqlStatementType;
import gudusoft.gsqlparser.TCustomSqlStatement;
import gudusoft.gsqlparser.stmt.TCreateTableSqlStatement;
import gudusoft.gsqlparser.nodes.TColumnDefinition;
import gudusoft.gsqlparser.TGSqlParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Iterator;

import org.apache.commons.cli.*;


public class MySQLToHiveDDL {

	public static void main(String args[]) throws Exception{
		
		// Add the CommandLine Options
		CommandLineParser parser = new GnuParser();
		Options options = new Options();
		options.addOption(OptionBuilder.withLongOpt("schemaFile").hasArg().withDescription("MySQL Schema File Location (S3 URL)").isRequired().create());		
		options.addOption(OptionBuilder.withLongOpt("hiveDDLFile").hasArg().withDescription("Hive DDL File Name").isRequired().create());
		options.addOption(OptionBuilder.withLongOpt("schemaName").hasArg().withDescription("Hive Schema Name").isRequired().create());
		options.addOption(OptionBuilder.withLongOpt("delim").hasArg().withDescription("Hive Row Format Field Delimiter").create());
		options.addOption(OptionBuilder.withLongOpt("serde").hasArg().withDescription("Hive Custom Serde - cannot be used with delim").create());
		options.addOption(OptionBuilder.withArgName( "serde property=value" ).hasArgs(2).withValueSeparator().withDescription( "use value for given property" ).create( "S" ));
		options.addOption(OptionBuilder.withLongOpt("hiveExternalRoot").hasArg().withDescription("External Hive Table Location (S3 URL)").isRequired().create());

		CommandLine line = null;
		try {
			line = parser.parse(options, args);
		} catch (Exception e) {
			System.err.println("Error parsing command line inputs.");
			System.err.println(e.getMessage());
			printUsage(options);
			System.exit(1);
		}
		String mySqlSchemaFileName = line.getOptionValue("schemaFile");
		String hiveDDLFileName = line.getOptionValue("hiveDDLFile");
		String schemaName = line.getOptionValue("schemaName");
		String delim = line.getOptionValue("delim");
		String serde = line.getOptionValue("serde");
		String serdeproperties = "";
		if (delim == null) {
			Iterator<String> i = line.getOptionProperties("S").stringPropertyNames().iterator();
			while (i.hasNext()) {
				String s = i.next();
				String c = i.hasNext() ? ", " : "";
				serdeproperties = serdeproperties + "\"" + s + "\"=\"" + line.getOptionProperties("S").getProperty(s) + "\"" + c;
			}
		}
		String hiveExternalLocationRoot = line.getOptionValue("hiveExternalRoot");
		
		
		TGSqlParser sqlparser = new TGSqlParser(EDbVendor.dbvmysql);
		
		StringBuffer DDLStrBuf = new StringBuffer("CREATE SCHEMA IF NOT EXISTS " + schemaName + ";\n");
		DDLStrBuf = DDLStrBuf.append("USE " + schemaName + ";\n");

		try {
			sqlparser.setSqlfilename(mySqlSchemaFileName);
			int ret = sqlparser.parse();

			if (ret == 0){
				for(int i=0;i<sqlparser.sqlstatements.size();i++){
					TCustomSqlStatement stmt = sqlparser.sqlstatements.get(i);  
					if (stmt.sqlstatementtype != ESqlStatementType.sstcreatetable){
						System.out.println("Skipping SQL as it is not a CREATE TABLE statement type: " + stmt.sqlstatementtype);
					} else {
						DDLStrBuf.append(generateHiveDDL((TCreateTableSqlStatement)stmt, hiveExternalLocationRoot, 
															delim, serde, serdeproperties));
					}  
				}
			}else{
				throw new Exception(sqlparser.getErrormessage());
			}
		} catch (Exception e) {
			System.err.println("Error parsing MySQL DDL!");
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Done generating DDL...");
		System.out.println("Starting dump to file...");
		// Write the hive DDL file:
		try {
			File f = new File(hiveDDLFileName);             
			if (!f.exists()) {
				f.createNewFile();
			}

			FileWriter fw = new FileWriter(f.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(DDLStrBuf.toString());
			bw.close();
			System.out.println("Done writting Hive DDL");
		} catch (Exception e) {
			System.err.println("Error writting hive DDL file");
			e.printStackTrace();
			System.exit(1);
		}
	}


	protected static StringBuffer generateHiveDDL(TCreateTableSqlStatement createStmt, String locationRoot, 
														String delim, String serde, String serdeproperties) {
		String tableName = createStmt.getTargetTable().toString().replaceAll("`","").toUpperCase();
		System.out.println("Generating DDL for: " + tableName);

		String location;
		if (locationRoot.endsWith("/")) {
			 location = locationRoot + tableName;
		} else {
			location = locationRoot + "/" + tableName;
		}		
		
		StringBuffer hiveDDL = new StringBuffer("CREATE EXTERNAL TABLE");
		hiveDDL = hiveDDL.append(" ");
		hiveDDL = hiveDDL.append(tableName);
		hiveDDL = hiveDDL.append("\n");

		hiveDDL = hiveDDL.append("(\n");

		TColumnDefinition column;
		for (int i=0; i<createStmt.getColumnList().size(); i++){
			column = createStmt.getColumnList().getColumn(i);
			hiveDDL.append(column.getColumnName().toString());
			hiveDDL.append(" ");
			hiveDDL.append(HiveTypes.toHiveType(column.getDatatype().toString()));
			if (i < createStmt.getColumnList().size()-1) {
				hiveDDL.append(", ");
			}
		}
		hiveDDL = hiveDDL.append("\n)\n");
		if (delim != null){
			hiveDDL = hiveDDL.append("ROW FORMAT DELIMITED FIELDS TERMINATED BY '"+ delim + "'");
		} else {
			hiveDDL = hiveDDL.append("ROW FORMAT SERDE '" + serde + "' \n");
			hiveDDL = hiveDDL.append("WITH SERDEPROPERTIES (\n ");
			hiveDDL = hiveDDL.append(serdeproperties);
			hiveDDL = hiveDDL.append("\n)");
		}
		hiveDDL = hiveDDL.append("\n");
		hiveDDL = hiveDDL.append("LOCATION '" + location + "';");
		hiveDDL = hiveDDL.append("\n");

		return hiveDDL;
	}

	static void printUsage(Options options) {
		
		/* Inputs:
		String mySqlSchemaFileName = args[0];
		String hiveDDLFileName = args[1];
		String schemaName = args[2];
		String delim = args[3];
		String hiveExternalLocation = args[2];
		
		System.err.println("Invalid inputs to main. Please pass the following arguments in order -- \n" 
				+ " mySqlSchemaFileName, hiveDDLFileName, schemaName, delimiter, hiveExternalLocation ");
		 */
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp( "java MySQLToHiveDDL --schemaFile=<s3://...> --hiveDDLFile=<tmp file name> --schemaName=default [--delim=\\t | --serde=<serde class>] --hiveExternalRoot=<s3://...>", options);
	}
}
